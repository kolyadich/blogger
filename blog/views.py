# -*- coding: utf-8 -*-
from django.contrib import auth
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response, get_object_or_404, redirect

from blog.forms import PostForm, LoginForm
from django.template import RequestContext
from models import Post, Blog, Follow, ReadPost


@login_required
def index(request):
    posts = Post.objects.all()
    return render(request, 'base.html', {'posts': posts})


# def register(request):
# form = RegisterForm(request.POST or None)
# if request.method == 'POST':
# username = request.POST.get('username', '')
#         password = request.POST.get('password', '')
#         email = request.POST.get('email', '')
#         if User.objects.filter(username=username):
#             return render(request, "register.html", {'errors': 'bad'})
#
#         user = User.objects.create_user(username=username, password=password, email=email)
#         if user:
#             user = authenticate(username=username, password=password, email=email)
#             auth.login(request, user)
#             return HttpResponseRedirect('/')
#         return render(request, str(user), {'form': form})
#     else:
#         return render(request, 'register.html', {'form': form})


def login(request):
    if request.method == 'POST':

        form = LoginForm(request.POST or None)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                auth.login(request, user)
                return HttpResponseRedirect('/')
            else:
                return render_to_response('login.html', {'form': form})
    else:
        form = LoginForm()
        return render(request, 'login.html', {'form': form})


def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/")


@login_required
def profile(request, pk=None):
    form = PostForm()
    user = request.user
    is_follow = False
    if pk:
        user = User.objects.get(id=pk)
        is_follow = Follow.objects.filter(follower=request.user, followee=user).exists()
    posts = Post.objects.filter(blog__author=user)
    users = User.objects.all().exclude(blog__author=user)
    followees = Follow.objects.filter(follower=user)
    return render(request, 'profile.html', {'user': user, 'posts': posts, 'users': users, 'followees': followees,
                                            'is_follow': is_follow, 'pk': pk, 'form': form},
                  context_instance=RequestContext(request))


@login_required
def follow(request, pk):
    user = get_object_or_404(User, id=pk)
    Follow.objects.get_or_create(followee=user, follower=request.user)
    return redirect('/')


@login_required
def unfollow(request, pk):
    user = get_object_or_404(User, id=pk)
    Follow.objects.filter(followee=user, follower=request.user).delete()
    return redirect('/')


@login_required
def feeds(request):
    user = request.user
    foll_ids = Follow.objects.values_list('followee_id', flat=True).filter(follower=user)
    post_foll = Post.objects.filter(blog__author_id__in=set(foll_ids))
    return render_to_response('feeds.html', {'post_foll': post_foll},
                              context_instance=RequestContext(request))


@login_required
def add_post(request):
    user = request.user
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            blog = Blog.objects.get(author=user)
            title = form.cleaned_data.get('title')
            description = form.cleaned_data.get('description')
            Post.objects.create(blog=blog, title=title, description=description)
        return HttpResponseRedirect('/profile/%s' % user.id)
    else:
        form = PostForm()
    return render(request, 'post_profile.html', {'form': form})


def view_post(request, post_id):
    one_post = get_object_or_404(Post, id=post_id)
    return render(request, 'view_post.html', {'one_post': one_post})


def post_read(request, pk, post_id):
    user = get_object_or_404(User, id=pk)
    post = get_object_or_404(Post, id=post_id)
    ReadPost.objects.get_or_create(read_post=post, read_user=user)
    # is_read = ReadPost.objects.filter(read_post=post, read_user=user)
    print(user, post_id)
    return redirect('/profile/%s' % user.id)


@login_required
def delete_post(request, post_id):
    user = request.user
    post_delete = get_object_or_404(Post, pk=post_id)
    post_delete.delete()
    return redirect('/profile/%s' % user.id)

