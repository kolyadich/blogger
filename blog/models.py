# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Blog(models.Model):
    title = models.CharField(max_length=200, default='Your blog')
    author = models.ForeignKey(User)

    def __unicode__(self):
        return self.author.username


def user_post_save(sender, instance, created, **kwargs):
    """Create a user blog when a new user account is created"""
    if created is True:
        blog = Blog()
        blog.author = instance
        blog.save()


post_save.connect(user_post_save, sender=User)


class Post(models.Model):
    blog = models.ForeignKey(Blog)
    title = models.CharField(max_length=200)
    description = models.TextField()
    date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return 'profile/%s' % self.blog.id


@receiver(post_save, sender=Post)
def post_post_save(sender, created, instance, **kwargs):
    if created:
        user = instance.blog.author
        followers_email = Follow.objects.filter(follower=user)

        for foll_email in followers_email:
            subject, from_email, to = 'New post', 'from@example.com', {foll_email.follower.email}
            text_content = u'New post'
            html_content = u'<p> {0} you have new post for user {1}, name post {2} {3}</p>'.format(
                user.username, foll_email.followee.username, instance.title, instance.get_absolute_url())
            msg = EmailMultiAlternatives(subject, text_content, from_email, {foll_email.follower.email})
            msg.attach_alternative(html_content, "text/html")
            msg.send()


post_save.connect(post_post_save, sender=Post)


class Follow(models.Model):
    follower = models.ForeignKey(User, related_name='posledovatel')
    followee = models.ForeignKey(User, related_name='posledovateli')


class ReadPost(models.Model):
    read_post = models.ForeignKey(Post)
    read_user = models.ForeignKey(User)
