from django import template
from blog.models import Follow, Post

register = template.Library()


@register.inclusion_tag('feeds.html', takes_context=True)
def feed_tag(context):
    request = context['request']
    user = request.user
    foll_ids = Follow.objects.values_list('followee_id', flat=True).filter(follower=user)
    post_foll = Post.objects.filter(blog__author_id__in=set(foll_ids))
    return {'post_foll': post_foll, 'request': context['request']}

