from django.contrib import admin
from blog.models import Post, Blog

admin.site.register(Post)
admin.site.register(Blog)