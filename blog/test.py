from blog.forms import PostForm
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.test import Client


class TestLogin(TestCase):

    def test_login_user(self):
        c = Client()
        response = c.post('/login/', {'username': 'john', 'password': 'smith'})
        self.assertEquals(response.status_code, 200)


# class TestForm(TestCase):
#
#     def test_form_post(self):
#         # c = Client()
#         # response = c.post('/add_post/', {'title': '1111', 'description': 'desc'})
#         # self.assertFormError(response, PostForm, field=None, errors=ValidationError, msg_prefix='Form_bad')
#         self.assertFieldOutput(CharField, {"text_good": 'Text good'}, {"&&&&&": ['Enter a valid email address.']})


