# coding: utf-8
from django import forms
from blog.models import Post, Blog
from django.forms import ModelForm


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))


class RegisterForm(forms.Form):
    username = forms.CharField(label='Логин', max_length=100, error_messages={'required': 'Укажите логин'})
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(),
                               error_messages={'required': 'Укажите пароль'})
    email = forms.EmailField(widget=forms.EmailInput)


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'description']

    def form_valid(self, form):
        form.instance.blog = self.request.blog
        return super(PostForm, self).form_valid(form)

