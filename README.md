# **Установка** #

*Если необходимо, установите недостающие пакеты* 



```

#!
sudo apt-get install python2.7 libpq-dev python-dev postgresql postgresql-contrib


```

# **Разворачивание проекта** #

*Backend*


```
#!
git clone git@bitbucket.org/kolyadich/blogger.git
cd blogger/
virtualenv .env 
source .env/bin/activate 
pip install -r requirements.txt 
python manage.py migrate 
python manage.py runserver

```