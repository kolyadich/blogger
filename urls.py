from django.conf.urls import include, url
from django.contrib import admin
from blog import views

urlpatterns = [

    url(r'^$', views.index, name="index"),
    url(r'^feeds/$', views.feeds, name='feeds'),

    url(r'^profile/(?P<pk>[0-9]+)/$', views.profile, name="profile"),
    url(r'^profile/(?P<post_id>[0-9]+)/delete/$', views.delete_post, name="delete_post"),
    url(r'^profile/follower/(?P<pk>[0-9]+)/$', views.follow, name="follow"),
    url(r'^profile/unfollower/(?P<pk>[0-9]+)/$', views.unfollow, name="unfollow"),

    url(r'^view_post/(?P<post_id>[0-9]+)/$', views.view_post, name="view_post"),
    url(r'^read_post/(?P<pk>[0-9]+)/(?P<post_id>[0-9]+)/$', views.post_read, name="read_post"),

    url(r'^add_post/$', views.add_post, name="add_post"),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^login/', views.login, name="login"),
    # url(r'^register/', views.register, name="register"),
    url(r'^logout/', views.logout, name="logout"),
]



